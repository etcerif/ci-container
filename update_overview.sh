#!/usr/bin/env bash

echo "Updating Docker Hub overview"

# Get a JWT from Docker Hub by first extracting username/password from the
# auth.json file provided.
IFS=':' read -ra docker_auth < <(jq -r '.auths["registry.hub.docker.com"].auth' auth.json | base64 -d)

# Make an API login call to get a JWT token back.
token=$(curl -sX POST \
  -H 'Content-type: application/json' \
  -d "{\"username\": \"${docker_auth[0]}\", \"password\": \"${docker_auth[1]}\"}" \
  https://hub.docker.com/v2/users/login/ | jq -r .token)

# API call to push README.md as JSON to Overview page.
return_code=$(jq -n --arg msg "$(<README.md)" \
  '{"registry":"registry-1.docker.io","full_description": $msg }' | \
  curl -D - -sL -w '%{http_code}' https://cloud.docker.com/v2/repositories/stemid/ci \
  -d @- -X PATCH \
  -H 'Content-Type: application/json' \
  -H "Authorization: JWT $token")

if [[ "${return_code}" = "200" ]]; then
  echo "Pushed README to Docker Hub Overview"
else
  echo "Failed to push README: ${return_code}"
fi
