#!/usr/bin/env bash

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

#set -o errexit
tag=$1

test -z "$tag" && exit 1

# Determine which image we base our own on.
if [ $tag = 'fedora' ]; then
  from_tag=registry.fedoraproject.org/fedora-minimal:32
elif [ $tag = 'alpine' ]; then
  from_tag=docker.io/alpine:3.12
else
  # Default tag, hopefully never used.
  from_tag=docker.io/alpine:3.12
fi

# Use some environment variables here so we can provide them in CI pipelines.
container=$(buildah from $from_tag)
registry_image=${REGISTRY_IMAGE:-registry.hub.docker.com/stemid/ci}
registry_tag=${REGISTRY_TAG:-$tag}

buildah config --label maintainer="Stefan Midjich <swehack at gmail.com>" $container
buildah config --entrypoint /bin/bash $container

# Run OS specific buildah commands.
bash "$tag.sh" $container

# Non-specific commands only after this.
test -f kubectl || \
  curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
buildah copy $container kubectl /usr/bin/kubectl
buildah run $container chmod 0755 /usr/bin/kubectl

test -f install_kustomize.sh || \
  curl -LO "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"
buildah copy $container install_kustomize.sh /opt/install_kustomize.sh

buildah run $container bash /opt/install_kustomize.sh
buildah run $container cp kustomize /usr/bin/kustomize

echo "$0: buildah commit"
buildah commit $container ci:$registry_tag

echo "$0: buildah push"
buildah push --authfile ./auth.json localhost/ci:$registry_tag docker://$registry_image:$registry_tag

# This only works if you login using your real credentials, not tokens or
# if you use 2FA. Useless in other words.
test -z "$UPDATE_DOCKER_OVERVIEW" || bash update_overview.sh
